burpsuite (2023.4.3-0kali1) kali-dev; urgency=medium

  * New upstream version 2023.4.3

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Tue, 16 May 2023 15:28:12 +0200

burpsuite (2023.3.5-0kali1) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * ci: Disable CI on this package as it's too big.

  [ Daniel Ruiz de Alegría ]
  * New upstream version 2023.3.5

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Sun, 23 Apr 2023 11:34:46 +0200

burpsuite (2023.1.2-0kali1) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Try GitLab-CI again

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Add RFP bugs in 1.5-1.
  * Update standards version to 4.6.2, no changes needed.

  [ Daniel Ruiz de Alegría ]
  * New upstream version 2023.1.2

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Tue, 21 Feb 2023 12:05:03 +0100

burpsuite (2022.12.5-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.12.5

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Tue, 03 Jan 2023 16:06:58 +0100

burpsuite (2022.12.4-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.12.4

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 19 Dec 2022 09:22:33 +0100

burpsuite (2022.11.4-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.11.4

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Thu, 08 Dec 2022 13:14:11 +0100

burpsuite (2022.9.6-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.9.6

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Thu, 10 Nov 2022 16:20:31 +0100

burpsuite (2022.9.5-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.9.5

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 04 Nov 2022 08:58:02 +0100

burpsuite (2022.8.5-0kali2) kali-dev; urgency=medium

  * Add superficial test

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Thu, 13 Oct 2022 10:04:48 +0200

burpsuite (2022.8.5-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.8.5

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 30 Sep 2022 10:12:40 +0200

burpsuite (2022.8.4-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.8.4

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Wed, 07 Sep 2022 09:02:45 +0200

burpsuite (2022.8.3-0kali2) kali-dev; urgency=medium

  * Escape `&` char in debian/watch file

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 05 Sep 2022 11:49:14 +0200

burpsuite (2022.8.3-0kali1) kali-dev; urgency=medium

  * New upstream version 2022.8.3

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Fri, 02 Sep 2022 15:34:38 +0200

burpsuite (2022.7.1-0kali1) kali-dev; urgency=medium

  * Finish debian/watch to download the correct file
  * Update debian/watch so that it only checks stable releases
  * New upstream version 2022.7.1

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Mon, 25 Jul 2022 21:13:59 +0200

burpsuite (2022.7-0kali1) kali-dev; urgency=medium

  * Add uscan script to create orig package
  * Update debhelper-compat
  * Add debian/watch
  * New upstream version 2022.7

 -- Daniel Ruiz de Alegría <daniruiz@kali.org>  Thu, 21 Jul 2022 14:12:41 +0200

burpsuite (2021.10.3-0kali1) kali-dev; urgency=medium

  * New upstream version 2021.10.3

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 13 Dec 2021 16:50:18 +0100

burpsuite (2021.10.2-0kali3) kali-dev; urgency=medium

  * Remove the content of debian/kali-ci.yml (source is too large)
  * Remove useless copies of Chromium

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 02 Dec 2021 11:37:39 +0100

burpsuite (2021.10.2-0kali2) kali-dev; urgency=medium

  * Fix the dependency required for the helper-script

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 30 Nov 2021 14:27:08 +0100

burpsuite (2021.10.2-0kali1) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Disable i386 build in CI
  * New upstream version 2021.10.2
  * Bump Standards-Version to 4.6.0
  * Fix helper-script: do not use exec

  [ Ben Wilson ]
  * Update email address
  * Add executable to helper-script
  * New helper-script format
  * Use java-wrappers
  * Update email address
  * Remove template comment and switch spaces to tabs

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 29 Nov 2021 16:40:15 +0100

burpsuite (2021.8.2-0kali1) kali-dev; urgency=medium

  * New upstream version 2021.8.2
  * Add an helper-script

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 25 Aug 2021 16:43:57 +0200

burpsuite (2021.2.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2021.2.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 22 Feb 2021 13:58:51 +0100

burpsuite (2020.12.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.12.1

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 06 Jan 2021 17:06:55 +0100

burpsuite (2020.9.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.9.1

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 09 Sep 2020 09:05:27 +0200

burpsuite (2020.8.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.8.1

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 26 Aug 2020 16:23:38 +0200

burpsuite (2020.8-0kali1) kali-dev; urgency=medium

  * Update Uploaders
  * New upstream version 2020.8

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 07 Aug 2020 18:09:50 +0200

burpsuite (2020.7-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.7

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 21 Jul 2020 12:29:54 +0200

burpsuite (2020.6-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.6

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 08 Jul 2020 11:03:49 +0200

burpsuite (2020.5.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.5.1

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 26 Jun 2020 17:28:24 +0200

burpsuite (2020.5-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.5

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 09 Jun 2020 14:48:48 +0200

burpsuite (2020.4-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.4
  * Bump Standards-Version to 4.5.0

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 07 May 2020 13:35:26 +0200

burpsuite (2020.2.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.2.1

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 09 Apr 2020 15:13:40 +0200

burpsuite (2020.1-0kali1) kali-dev; urgency=medium

  * New upstream version 2020.1

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 04 Feb 2020 17:34:41 +0100

burpsuite (2.1.07-0kali3) kali-dev; urgency=medium

  * Add arm64

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 20 Jan 2020 12:14:12 +0100

burpsuite (2.1.07-0kali2) kali-dev; urgency=medium

  * Restrict to amd64

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 20 Dec 2019 16:18:51 +0100

burpsuite (2.1.07-0kali1) kali-dev; urgency=medium

  * New upstream version 2.1.07

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 19 Dec 2019 14:47:47 +0100

burpsuite (2.1.04-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 2.1.04

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 16 Oct 2019 08:31:38 +0200

burpsuite (2.1.02-0kali1) kali-dev; urgency=medium

  * New upstream version 2.1.02

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 16 Aug 2019 11:32:16 +0200

burpsuite (2.1.01-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Add GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 2.1.01

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 22 Jul 2019 14:03:29 +0200

burpsuite (2.1-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com

  [ Sophie Brun ]
  * Add debian/gbp.conf
  * New upstream version 2.1

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Jul 2019 11:17:05 +0200

burpsuite (1.7.36-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 20 Aug 2018 11:29:39 +0200

burpsuite (1.7.35-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 02 Jul 2018 14:08:09 +0200

burpsuite (1.7.34-0kali1) kali-dev; urgency=medium

  * Import new usptream release

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 15 Jun 2018 09:19:55 +0200

burpsuite (1.7.33-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 11 Apr 2018 18:15:10 +0200

burpsuite (1.7.32-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 06 Mar 2018 09:35:04 +0100

burpsuite (1.7.30-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 12 Jan 2018 13:39:22 +0100

burpsuite (1.7.27-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 17 Oct 2017 15:08:23 +0200

burpsuite (1.7.26-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 09 Aug 2017 14:42:28 +0200

burpsuite (1.7.21-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 18 Apr 2017 14:23:09 +0200

burpsuite (1.7.17-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 23 Feb 2017 09:35:42 +0100

burpsuite (1.7.03-0kali1) kali-dev; urgency=medium

  * Import new upstream release (Closes: 0003292)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 17 May 2016 14:20:45 +0200

burpsuite (1.6.32-0kali1) kali-dev; urgency=medium

  * Import new upstream release (Closes: 0002954)

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 30 Dec 2015 19:03:50 +0100

burpsuite (1.6.31-0kali0) kali-dev; urgency=medium

  * Imported new upstream release (Closes: 0002873)

 -- Devon Kearns <dookie@kali.org>  Wed, 02 Dec 2015 11:00:06 -0700

burpsuite (1.6.30-0kali1) sana; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 17 Nov 2015 15:16:14 +0100

burpsuite (1.6.01-0kali0) kali; urgency=low

  * Imported new upstream release (Closes: 0002277)

 -- Devon Kearns <dookie@kali.org>  Fri, 15 May 2015 10:41:25 -0600

burpsuite (1.6-0kali2) kali; urgency=medium

  * Added jarwrapper as an install depend

 -- Devon Kearns <dookie@kali.org>  Mon, 26 Jan 2015 09:30:10 -0700

burpsuite (1.6-0kali1) kali; urgency=low

  * Imported new upstream release (Closes: 0001152)

 -- Devon Kearns <dookie@kali.org>  Tue, 15 Apr 2014 09:47:02 -0600

burpsuite (1.5-1kali4) kali; urgency=low

  * Removed desktop file

 -- Mati Aharoni <muts@kali.org>  Fri, 14 Dec 2012 19:52:24 -0500

burpsuite (1.5-1kali3) kali; urgency=low

  * Fixing desktop file path

 -- Mati Aharoni <muts@kali.org>  Sat, 01 Dec 2012 09:24:57 -0500

burpsuite (1.5-1kali2) kali; urgency=low

  * Updating desktop file

 -- Mati Aharoni <muts@kali.org>  Sat, 01 Dec 2012 09:13:36 -0500

burpsuite (1.5-1kali1) kali; urgency=low

  * Kali Version

 -- Devon Kearns <dookie@kali.org>  Tue, 06 Nov 2012 15:16:40 -0700

burpsuite (1.5-1) kali; urgency=low

  * Initial release. Closes: #832943

 -- Devon Kearns <dookie@kali.org>  Tue, 06 Nov 2012 14:57:41 -0700
